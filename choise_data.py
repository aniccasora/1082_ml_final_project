# 把 Data/filterd_img 內的資料 0~9999 抽出來
# 另外放到 img_0_9999
import os, sys, csv
import for_data_prepare.ocr_gogo as myocr

if __name__ == "__main__":
    img_export_folder = r'./data/img_0_9999'
    filtered_img_folder = r'./data/img_0_9999'
    target_folder = r'./data/filterd_img'
    csv_filename = r'label_0_9999.csv'
    prefix_fn = 'c_'
    fn_fill_zero = 5
    MAX_NO_REPEAT_DATA_AMOUNT = 2000 # 不重複的 多少筆資料
    if not os.path.exists(r'./data/filterd_img'):
        sys.exit(r'Fatal: "./data/filterd_img" directory found!')



    # 先在目錄建立 label.csv，(全新的檔案，只有標題)
    with open('./data/' + csv_filename, 'w', newline='') as csvfile:
        # 建立 CSV 檔寫入器
        writer = csv.writer(csvfile)

        # 只寫入標題
        writer.writerow(['filename', 'label'])
        csvfile.flush()

    cnt = 1
    chk_list = []
    # 走訪 目標資料夾下的所有檔案
    for filename in os.listdir(target_folder):

        if len(chk_list) > MAX_NO_REPEAT_DATA_AMOUNT: # 有10000個不重複內容圖片後終止迴圈
            break

        reco_number = myocr.captcha2str(target_folder + '\\' + filename)

        if len(reco_number) != 4:
            continue  # 辨識出來的東西 不是四位數 直接下一筆

        if (reco_number in chk_list): # 檢查是否辨識過
            continue
        else:
            chk_list.append(reco_number) # 沒辨識過就加入list，等待之後檢查


        # 辨識出來的 數字(reco_number)，先寫到csv內。
        with open('./data/' + csv_filename, 'a', newline='') as csvfile:
            # 建立 csv 寫入器
            writer = csv.writer(csvfile)
            # 寫入 [檔案名稱，辨識出來的結果]
            writer.writerow([filename[0:filename.rfind('.')], reco_number])
            csvfile.flush()
        # csv 寫入完成。

        no_labelInfo_name = prefix_fn + str(cnt).zfill(fn_fill_zero) + ".png"
        cnt += 1

        # 開檔案 開來源檔案(讀取原始圖片)
        with open(target_folder + "\\" + filename, 'rb') as ifp:
            # 讀資料, 因為這個是二進位檔案要寫到別的地方所以要用
            # bytearray表明他是要記成 二進位
            binary_format = bytearray(ifp.read())

            # 另存到 濾過資料夾(有被合法辨識的 img，且只存放原始檔案名稱)
            with open(filtered_img_folder + "\\" + filename, 'wb') as offp:
                offp.write(binary_format)
                offp.flush()
    # end for
    print("Total labeled file :", str(cnt))

