import myutil as util
from PIL import Image
import numpy as np
import tensorflow as tf
import csv
from sklearn.model_selection import train_test_split # 拆 訓練、測試資料用。
import matplotlib.pyplot as plt

LETTERSTR = "0123456789"


def load_weight_and_test_model(image_pth: [], model_h5_pth, \
                               show_detail = False, FORCE_USE_CPU = True):

    # 強制使用 cpu，其原理是把 gpu list 假裝為空，所以就會去用 cpu 了。
    if FORCE_USE_CPU:
        tf.config.experimental.set_visible_devices([], 'GPU')

    answer_list = []

    for each_image_pth in image_pth:
        v_model = util.get_my_model() # 拿模型
        v_model.load_weights(model_h5_pth)# 套上權重
        print(" =============== Go predict ===============")
        ximage = Image.open(each_image_pth)
        ximage = ximage.convert("RGB")
        ximage = np.asarray(ximage) / 255.0
        ximage = np.asarray([ximage])  # 3d to 4d
        result = v_model.predict(ximage)
        answer = ""
        fig = plt.figure(figsize=(14,4))
        # plt.subplots_adjust(left=)
        for i, r in enumerate(result):
            answer += LETTERSTR[np.argmax(r[0])]
            if show_detail:
                plt.subplot(1,4,i+1)
                x = [ _ for _ in range(10)]
                plt.title("Digit_"+ str(i+1) + " = "+str(LETTERSTR[np.argmax(r[0])]))
                plt.xticks(x)
                plt.bar(x, r[0].tolist())

                print("digit_"+str(i+1)+"_onehot result:")
                print(r[0])
                print(" ===========================================")
        if show_detail:
            plt.show()
        answer_list.append(answer)
        #print("predict answer:", answer)
    print(answer_list)
    return answer_list

if __name__ == "__main__":
    # go_test
    # 需要全重，圖片，label。
    v_model = util.get_my_model()  # 拿模型
    v_model.load_weights("./train_finish_model/sim_model.h5")  # 套上權重
    v_model.summary()
    print("now please wiat preparing data...")

    test_images_folder = "./data/filterd_img"
    label_cvs_file = "./data/label.csv"

    images = []  # 紀錄 圖片
    labels = []  # 紀錄 label，兩者順序是對應的
    with open(label_cvs_file, 'r', newline='') as csvfile:
        rows = csv.reader(csvfile)
        next(rows)  # 略過第一行
        cnt = 1
        for row in rows:
            image = Image.open(test_images_folder + '/' + row[0] + '.png')
            image = image.convert("RGB")
            image = np.asarray(image) / 255.0
            images.append(image)
            labels.append(row[1])

    # images ==> [ ['3d_ndarray'], ... , ['3d_ndarray']]
    images = np.asarray(images)  # 轉成 4d ndarray，神經網路是吃"1"個大 4-dim 的 array。

    # 原本 labels 資料 是 list [ 1234,4568,4533, ... , 4564] ， 要轉換成 one-hot
    labels_onehot = []
    for label in labels:
        labels_onehot.append(util.toonehot(label))  # toonehot 自己寫的
    # 轉好 onehot 了
    labels_onehot = np.asarray(labels_onehot)

    # 我們的 model 有 4個輸出，所以有 4套 labels 對應不同輸出
    test_labels = [[] for _ in range(4)]  # 4 是因為驗證碼是 4位數。

    # 開始給數值瞜
    for idx, onehot in enumerate(labels_onehot):
        test_labels[0].append(onehot[0])
        test_labels[1].append(onehot[1])
        test_labels[2].append(onehot[2])
        test_labels[3].append(onehot[3])

    # test_labels 內元素轉 ndarray
    test_labels = [arr for arr in np.asarray(test_labels)]

    print(" ============ Total images ============ ")
    print("images.shape:", images.shape)
    print("test_labels[0]:", test_labels[0].shape)
    print(" ====================================== ")

    print("Start evaluating...")
    results = v_model.evaluate(images, test_labels,\
                     batch_size=1000)
    print("test loss, test acc:", results)