import myutil as util
from PIL import Image
import numpy as np
import tensorflow as tf
import csv
from sklearn.model_selection import train_test_split # 拆 訓練、測試資料用。

# 沒顯示卡，或太爛 就用這個參數組去做 假訓練
MAX_OF_DATA = 2000
split_idx = 1800 # split_idx 筆拿來做訓練
train_batch_size = 10
evaluate_batch_size = 10
train_epochs = 50
# ============================
# 下面是真實訓練所採用的參數
# all = 56234
# MAX_OF_DATA = 56234
# split_idx = 50000 # split_idx 筆拿來做訓練
# train_batch_size = 512
# evaluate_batch_size = 1000
# train_epochs = 50
# ============================

def train_and_save_model(train_image_pth=r"./data/filterd_img",
                         label_cvs_file = './data/label.csv',
                         save_model_name = "aa1",
                         FORCE_USE_CPU = True):
    # 強制使用 cpu，其原理是把 gpu list 假裝為空，所以就會去用 cpu 了。
    if FORCE_USE_CPU:
        tf.config.experimental.set_visible_devices([], 'GPU')


    images = [] # 紀錄 圖片
    labels = [] # 紀錄 label，兩者順序是對應的
    with open(label_cvs_file, 'r',newline='') as csvfile:
        rows = csv.reader(csvfile)
        next(rows) # 略過第一行
        cnt = 1
        for row in rows:
            image = Image.open(train_image_pth+'/'+row[0]+'.png')
            image = image.convert("RGB")
            image = np.asarray(image)/255.0
            images.append(image)
            labels.append(row[1])
            #print(image.shape)
            cnt+=1
            if cnt>MAX_OF_DATA:
                break

    # images ==> [ ['3d_ndarray'], ... , ['3d_ndarray']]
    images = np.asarray(images) # 轉成 4d ndarray，神經網路是吃"1"個大 4-dim 的 array。
    train_images = images[:split_idx]
    test_images = images[split_idx:]
    print(" ============ Total images ============ ")
    print("images.dtype:",images.dtype ,", images.shape:",images.shape)
    print(" ====================================== ")
    # 原本 labels 資料 是 list [ 1234,4568,4533, ... , 4564] ， 要轉換成 one-hot
    labels_onehot = []
    for label in labels:
        labels_onehot.append(util.toonehot(label)) #  toonehot 自己寫的
    # 轉好 onehot 了
    labels_onehot = np.asarray(labels_onehot)
    # onehot-encoding (以 2534 為例子):
    #
    # onehot = [[[0 0 1 0 0 0 0 0 0 0]   = 神經元 digit-1 的輸出
    #            [0 0 0 0 0 1 0 0 0 0]   = 神經元 digit-2 的輸出
    #            [0 0 0 1 0 0 0 0 0 0]   = 神經元 digit-3 的輸出
    #            [0 0 0 0 1 0 0 0 0 0]]] = 神經元 digit-4 的輸出

    # 第一筆資料 2534 會被拆分成
    # onehot[0] = [ [0 0 1 0 0 0 0 0 0 0], ... ... ]: 2
    # onehot[1] = [ [0 0 0 0 0 1 0 0 0 0], ... ... ]: 5
    # onehot[2] = [ [0 0 0 1 0 0 0 0 0 0], ... ... ]: 3
    # onehot[3] = [ [0 0 0 0 1 0 0 0 0 0], ... ... ]: 4

    # 我們的 model 有 4個輸出，所以有 4套 labels 對應不同輸出
    train_labels = [ [] for _ in range(4)] # 4 是因為驗證碼是 4位數。
    test_labels = [[] for _ in range(4)]  # 4 是因為驗證碼是 4位數。
    # 開始給數值瞜
    for idx,onehot in enumerate(labels_onehot):
        if idx < split_idx:
            train_labels[0].append(onehot[0])
            train_labels[1].append(onehot[1])
            train_labels[2].append(onehot[2])
            train_labels[3].append(onehot[3])
        else:
            test_labels[0].append(onehot[0])
            test_labels[1].append(onehot[1])
            test_labels[2].append(onehot[2])
            test_labels[3].append(onehot[3])
    # train_labels 內元素轉 ndarray
    train_labels = [arr for arr in np.asarray(train_labels)]
    test_labels = [arr for arr in np.asarray(test_labels)]
    # =====================================================

    # =====================================================
    print("train_images.shape = ", train_images.shape)
    print("test_images.shape = ", test_images.shape)
    print("train_labels[0].shape = ", train_labels[0].shape)
    print("test_labels[0].shape = ", test_labels[0].shape)
    print(" ============ Data prepare complete ============ ")

    # 用於儲存最佳辨識率的模型，每次epoch完會檢查一次，
    # 如果比先前最佳的acc高，就會儲存model。 先每次都存放
    checkpoint = tf.keras.callbacks. \
        ModelCheckpoint(filepath='./model_save/', save_weights_only=True,\
                        verbose=1)#, mode='auto',period=5)
        # ModelCheckpoint('./model_save', monitor='val_digit4_acc',\
        #                 verbose=1, save_best_only=True, mode='max')


    # 這邊的 monitor 設為 val_digit4_acc，patience設為5，
    # 也就是在驗證集的 val_digit4_acc 連續10次不再下降時，就會提早結束訓練。
    earlystop = tf.keras.callbacks.\
        EarlyStopping(monitor='val_digit4_accuracy', \
                              patience=10, verbose=1, mode='auto')

    tensorBoard = tf.keras.callbacks.\
        TensorBoard(log_dir="./logs/", histogram_freq=1)

    callbacks_list = [tensorBoard, earlystop, checkpoint]

    model = util.get_my_model()

    model.fit(train_images, train_labels, batch_size=train_batch_size, epochs=train_epochs,\
              verbose=2, validation_data=(test_images, test_labels),\
              callbacks= callbacks_list)

    results = model.evaluate(test_images, test_labels, batch_size=evaluate_batch_size)
    print("test loss, test acc:", results[:2])
    model.save(filepath='./train_finish_model/'+ save_model_name +'.h5')

if __name__ == "__main__":
    # 預設參數屌練一波
    # train_and_save_model()

    # 記得條最上面參數
    # 使用模擬 2000 訓練 (最多 2000 筆不重複資料下去練，比較符合真實情形)
    train_and_save_model(train_image_pth=r"./data/img_0_2000",
                         label_cvs_file = './data/label_0_2000.csv',
                         save_model_name = "sim_model",
                         FORCE_USE_CPU = True)