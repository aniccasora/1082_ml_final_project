from train_model import train_and_save_model # 練 model 的 code
from test_model import load_weight_and_test_model # 載入權重檔案並預測 結果
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from PIL import Image
from time import sleep

w_width, w_hight = 800, 500
test_time = 60
if __name__ == "__main__":

    # 開始訓練，並將模型存下
    # train_and_save_model()

    options = Options()
    # 如果不想看自動操作取消下面2行註解
    # options.add_argument('--headless')
    # options.add_argument('--no-sandbox')
    # 設定chromedriver路徑
    driver = webdriver.Chrome(r'./chromedriver', chrome_options=options)
    # 設定視窗大小很重要，可能會影響判讀
    driver.set_window_size(w_width, w_hight)
    for test_t in range(test_time):
        # 使用 driver 開起 portal
        driver.get("https://portalx.yzu.edu.tw/PortalSocialVB/Login.aspx")
        # 將目前頁面存到 portal.png
        driver.save_screenshot(r'./chromedriver_tmp/login.png')
        # 取子圖的相對座標，這個用 picpick 框一框就知道了，
        x, y, w, h = 511, 246, 140, 56
        hi = Image.open(r'./chromedriver_tmp/login.png')  # 存下登入畫面
        captcha = hi.crop((x, y, x + w, y + h))  # 從登入的畫面切下 一個子圖，會剛好框出驗證碼
        resize_captcha = captcha.resize(size=(112, 45))  # 將驗證碼縮放至 神經網路的 input
        resize_captcha.save(r'./chromedriver_tmp/captcha.png')
        print(r"save image: './chromedriver_tmp/captcha.png'")

        image_pth = ['./chromedriver_tmp/captcha.png']
        model_pth = './train_finish_model/1080Ti_train.h5' # 要載入的 權重。

        predict = load_weight_and_test_model(image_pth, model_h5_pth=model_pth, show_detail= True)

        inputs = driver.find_element_by_xpath('//*[@id="Txt_VeriCode"]')
        inputs.send_keys(predict[0])
        sleep(0.3)