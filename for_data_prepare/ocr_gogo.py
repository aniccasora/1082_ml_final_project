from PIL import Image
import pytesseract
import re

# 吃一張圖的路徑，給出 他圖片內的數字，不是數字的一律會在輸出中被忽略。
# 輸入的圖片要求，他要是 一行 的數字才能。
# --psm N: 設定判讀的圖片的 layout。
# --oem N: 指定 OEM Engine。
# 詳細可以參考: https://github.com/tesseract-ocr/tesseract/blob/master/doc/tesseract.1.asc
def captcha2str(img_pth, config="--psm 7 --oem 3"):
    img = Image.open(img_pth)

    width, height = img.size
    r = 0.5
    newsize = ( int(width*r), int(height*r))
    img = img.resize(newsize)

    # 判讀摟
    rtn = pytesseract.image_to_string(img, config=config)
    # 非數字的字元 全部 erase。
    rtn = re.sub('[^0-9]', '', rtn)
    return (rtn)
