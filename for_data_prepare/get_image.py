import requests
from fake_useragent import UserAgent
from time import sleep
import os
import sys
from ocr_gogo import captcha2str
import pytesseract
import csv

# 爬驗證碼並存下來用
def getCaptcha(num_of_img = 1, path="./data/img/", start_num=1,
               fill_zero = 5, each_sleep_sec = 0.03, filename_prefix="c_"):
    # 取得 假user
    ua = UserAgent()

    # 驗證碼 url
    url = 'https://portalx.yzu.edu.tw/PortalSocialVB/SelRandomImage.aspx'

    # 生 agent
    user_agent = ua.chrome

    # 檔名計數器
    name_cnt = start_num
    for req_time in range(num_of_img):
        try:
            res = requests.get(url, headers={'user-agent': user_agent}, timeout = 0.5)
        except:
            print("time out... Go retry...")
            sleep(1) # 固定睡覺1秒
            continue

        # 串檔名
        filname = filename_prefix+str(name_cnt).zfill(fill_zero)+".png"

        # 從 response 取得 data，並存下來。
        with open(path+filname, 'wb') as f:
            # iter_content(): Iterates over the response data.
            # 這種 不用　text 讀取內容的原因 我們要及時取得 server 傳過來的 raw data。
            # 影片的取得也會用這個方法 iter_content。
            for chunk in res.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    f.flush() # 將緩存區的東西 強制寫到文件內
            f.close()
            print(filname, " OK!")

        # 準備下一次
        name_cnt += 1
        # 記得睡覺
        sleep(each_sleep_sec)


    # for end


# 將 target_folder 下的圖片 重新用OCR 加上標籤資訊再存放到 export_folder
def label_data_img(start_cnt = 1,
    fn_fill_zero = 5,
    target_folder = r'.\data\img',
    export_folder = r'.\data\labeled_img',
    filtered_img_folder = r'.\data\filterd_img',
    prefix_fn = "c_"):

    if not os.path.exists(r'.\data'):
        sys.exit(r'Fatal: "data" directory found!')

    # 檢查是否存在輸出資料夾
    try:
        if not os.path.exists(export_folder):
            print("create new folder :" + export_folder)
            os.makedirs(export_folder, 0o777)
    except OSError:
        sys.exit('Fatal: output directory "' + export_folder + '" does not exist and cannot be created')

    # 檢查是否存在輸出資料夾 filtered_img_folder
    try:
        if not os.path.exists(filtered_img_folder):
            print("create new folder :" + filtered_img_folder)
            os.makedirs(filtered_img_folder, 0o777)
    except OSError:
        sys.exit('Fatal: output directory "' + filtered_img_folder + '" does not exist and cannot be created')

    # 先在上層目錄建立 label.csv，(全新的檔案，只有標題)
    with open(export_folder + '\\..\\label.csv', 'w', newline='') as csvfile:
        # 建立 CSV 檔寫入器
        writer = csv.writer(csvfile)

        # 只寫入標題
        writer.writerow(['filename', 'label'])
        csvfile.flush()


    cnt = start_cnt
    # 走訪 目標資料夾下的所有檔案
    for filename in os.listdir(target_folder):

        reco_number = captcha2str(target_folder + '\\' + filename)

        if len(reco_number) != 4:
            continue # 辨識出來的東西 不是四位數 直接下一筆

        #辨識出來的 數字(reco_number)，先寫到csv內。
        with open(export_folder+'\\..\\label.csv', 'a', newline='') as csvfile:
            # 建立 csv 寫入器
            writer = csv.writer(csvfile)
            # 寫入 [檔案名稱，辨識出來的結果]
            writer.writerow([filename[0:filename.rfind('.')], reco_number])
            csvfile.flush()
        # csv 寫入完成。

        new_name = prefix_fn + reco_number + '_' + filename[filename.find('_')+1:]
        no_labelInfo_name = prefix_fn + str(cnt).zfill(fn_fill_zero) + ".png"
        print(filename, " = ", new_name)
        cnt += 1

        # 開檔案 開來源檔案(讀取原始圖片)
        with open(target_folder + "\\" + filename, 'rb') as ifp:
            # 讀資料, 因為這個是二進位檔案要寫到別的地方所以要用
            # bytearray表明他是要記成 二進位
            binary_format = bytearray(ifp.read())
            # 寫到目的地資料夾，檔案名稱會附加 label.
            with open(export_folder + "\\" + new_name, 'wb') as ofp:
                ofp.write(binary_format)
                ofp.flush()
            # 另存到 濾過資料夾(有被合法辨識的 img，且只存放原始檔案名稱)
            with open(filtered_img_folder + "\\" + filename, 'wb') as offp:
                offp.write(binary_format)
                offp.flush()
    # end for
    print("Total labeled file :", str(cnt))

if __name__ == "__main__":

    label_data_img()



