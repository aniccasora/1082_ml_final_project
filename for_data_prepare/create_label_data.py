import os,sys


# 將 target_folder 下的圖片 取得檔案名稱
def label_data_img(start_cnt = 1,
    fn_fill_zero = 5,
    target_folder = r'.\data\img',
    export_folder = r'.\data\labeled_img',
    prefix_fn = "c_"):

    if not os.path.exists(r'.\data'):
        sys.exit('Fatal: data directory found!')

    # 檢查是否存在輸出資料夾
    try:
        if not os.path.exists(export_folder):
            print("create new folder :" + export_folder)
            os.makedirs(export_folder, 0o777)
    except OSError:
        sys.exit('Fatal: output directory "' + export_folder + '" does not exist and cannot be created')

    cnt = start_cnt
    for filename in os.listdir(target_folder):
        new_name = captcha2str(target_folder + '\\' + filename)
        new_name = prefix_fn + new_name + '_' + str(cnt).zfill(fn_fill_zero) + ".png"
        print(filename, " = ", new_name)
        cnt += 1

        # 開檔案 開來源檔案
        with open(target_folder + "\\" + filename, 'rb') as ifp:
            # 讀資料, 因為這個是二進位檔案要寫到別的地方所以要用
            # bytearray表明他是要記成 二進位
            binary_format = bytearray(ifp.read())
            # 寫到目的地資料夾
            with open(export_folder + "\\" + new_name, 'wb') as ofp:
                ofp.write(binary_format)
                ofp.flush()
    # end for
    print("Labeled file :", str(cnt))