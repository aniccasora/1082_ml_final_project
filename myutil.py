# encoding: utf-8
import tensorflow as tf
from PIL import Image
import os
import glob
import random
import numpy as np
LETTERSTR = "0123456789"

# 驗證碼的 one hot coding
def toonehot(text):
    labellist = []
    for letter in text:
        onehot = [0 for _ in range(10)]
        num = LETTERSTR.find(letter)
        onehot[num] = 1
        labellist.append(onehot)

    return labellist


# 從 folder 中取得 image
def get_rand_img(target_folder=r".\data\labeled_img\*.png", showInfo=False):
    fl = glob.glob(target_folder)
    len_of_all = len(fl)  # 檔案數量
    if showInfo:
        print("Number of images :", str(len_of_all))
    pick_idx = random.randint(0, len_of_all - 1)  # 要選出的索引

    image = Image.open(fl[pick_idx])
    if showInfo:
        print("Get file: ", "\"" + fl[pick_idx][fl[pick_idx].rfind('\\') + 1:], end="\", ")
        print("format:", "\"" + image.format, end="\", ")
        print("size:", image.size, end=", ")
        print("mode:", image.mode)

    return image.convert('RGB')


def get_my_model():
    inl = tf.keras.layers.Input((45, 112, 3))
    out = inl
    out = tf.keras.layers.Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='relu')(out)
    out = tf.keras.layers.Conv2D(filters=32, kernel_size=(3, 3), padding='same',activation='relu')(out)
    out = tf.keras.layers.BatchNormalization()(out)
    out = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(out)
    out = tf.keras.layers.Dropout(0.3)(out)
    out = tf.keras.layers.Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='relu')(out)
    out = tf.keras.layers.Conv2D(filters=64, kernel_size=(3, 3), padding='same',activation='relu')(out)
    out = tf.keras.layers.BatchNormalization()(out)
    out = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(out)
    out = tf.keras.layers.Dropout(0.3)(out)
    out = tf.keras.layers.Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='relu')(out)
    out = tf.keras.layers.Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='relu')(out)
    out = tf.keras.layers.BatchNormalization()(out)
    out = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(out)
    out = tf.keras.layers.Dropout(0.3)(out)
    out = tf.keras.layers.Flatten()(out)
    out = tf.keras.layers.Dropout(0.3)(out)
    out = [tf.keras.layers.Dense(10, name='digit1', activation='softmax')(out),\
           tf.keras.layers.Dense(10, name='digit2', activation='softmax')(out),\
           tf.keras.layers.Dense(10, name='digit3', activation='softmax')(out),\
           tf.keras.layers.Dense(10, name='digit4', activation='softmax')(out)]
    myModel = tf.keras.Model(inputs= inl, outputs = out)

    myModel.compile(optimizer='adam',
                    loss='categorical_crossentropy',
                    metrics=['accuracy'])

    return myModel

# 檢查 cuda
def chk_is_built_with_cuda():
    print(tf.test.is_built_with_cuda())
